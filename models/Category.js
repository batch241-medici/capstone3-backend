const mongoose = require("mongoose");

const categorySchema = new mongoose.Schema({
	categoryName: {
		type: String,
		required: [true, "First Name is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	products: [
	{
		productId: {
			type: String,
			required: [true, "Product Id is required"]
		}
	}
	]
})

module.exports = mongoose.model("Category", categorySchema);