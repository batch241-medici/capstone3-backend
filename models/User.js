const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	fullName: {
		type: String,
		required: [true, "First Name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile No is required"]
	},
	addressId: {
		type: String,
		required: [false, "Address Id is required"]
	},
	orders: [
	{
		productId: {
			type: String,
			required: [true, "Product Id is required"]
		}
	}
	]
})

module.exports = mongoose.model("User", userSchema);