const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
	order: [
	{
		orderId: {
			type: String,
			required: [true, "Order Id is required"]
		},
		productId: {
			type: String,
			required: [true, "Product Id is required"]
		},
		quantity: {
			type: Number,
			required: [true, "Product quantity is required"]
		},
		subTotal: {
			type: Number,
			required: [true, "Subtotal price is required"]
		}
	},
	{
		totalPrice: {
			type: Number,
			required: [true, "Total price is required"]
		}
	}
	]
})

module.exports = mongoose.model("Cart", cartSchema);