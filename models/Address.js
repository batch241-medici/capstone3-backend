const mongoose = require("mongoose");

const addressSchema = new mongoose.Schema({
	buildingNo: {
		type: Number,
		required: [true, "Building Number is required"]
	},
	street: {
		type: String,
		required: [true, "Street Name is required"]
	},
	City : {
		type: String,
		required: [true, "City is required"]
	},
	Zipcode: {
		type: Number,
		required: [true, "Zip Code is required"]
	},
	userId: {
		type: String,
		required: [true, "User Id is required"]
	}
})

module.exports = mongoose.model("Address", addressSchema);