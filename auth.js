const jwt = require("jsonwebtoken");

const access = "Ecommerce";

// Token Creation
module.exports.createAccessToken = (user) => {

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	return jwt.sign(data, access, {})
};

// Token Verification
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;
	if(typeof token !== "undefined") {
		token = token.slice(7, token.length);

		return jwt.verify(token, access, (error, data) => {
			if(error) {
				return res.send({auth: "Failed"})
			} else {
				next();
			}
		})
	} else {
		return res.send({auth: "Failed"})
	}
};

// Token Decryption
module.exports.decode = (token) => {
	if(typeof token !== "undefined") {
		token = token.slice(7, token.length);

		return jwt.verify(token, access, (error, data) => {
			if(error) {
				return null
			} else {
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	} else {
		return null
	}
};