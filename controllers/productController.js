const Product = require("../models/Product");

// All Products
module.exports.allProduct = async () => {
	return await Product.find({}).then(result => {
		return result
	});
};

// // All active products
module.exports.allActive = async () => {
	return await Product.find({isActive: true}).then(result => {
		return result
	})
};

// Create a Product
module.exports.createProduct = async (reqBody) => {
	let newProduct = new Product({
		productName: reqBody.productName,
		description: reqBody.description,
		price: reqBody.price,
		quantity: reqBody.quantity
	});

	let checkIfProductExist = await Product.findOne({productName: reqBody.productName}).then(result => {
		if(result) {
			return true
		} else {
			return false
		}
	})

	if(checkIfProductExist === false) {
		return newProduct.save().then((result, error) => {
			if(error) {
				return false
			} else {
				return result
			}
		})
	}
};

// Retrieve a single product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
};

// Update Product
module.exports.updateProduct = (reqParams, reqBody) => {
	let updateProduct = {
		productName: reqBody.productName,
		description: reqBody.description,
		price: reqBody.price,
		quantity: reqBody.quantity
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((result, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
};

// Change status of product
module.exports.changeStatusOfProduct = (reqParams, reqBody) => {
	let changeStatus = {
		isActive: reqBody.isActive
	}

	return Product.findByIdAndUpdate(reqParams.productId, changeStatus).then((result, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
};

