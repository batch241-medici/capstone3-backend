const express = require("express");

const router = express.Router();

const auth = require("../auth");

const userController = require("../controllers/userController");

// Checking if user's email already exist
router.post("/checkEmail", (req, res) => {
	userController.checkEmail(req.body).then(resultFromController => res.send(resultFromController));
});

// User Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// User Login Authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// User Details
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
		console.log(req.headers.authorization);
	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});

// All Users
router.get("/allUsers", (req, res) => {
	userController.allUser().then(resultFromController => res.send(resultFromController));
});


module.exports = router;