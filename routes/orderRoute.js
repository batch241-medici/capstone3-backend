const express = require("express");

const router = express.Router();
const orderController = require("../controllers/orderController");
const auth = require("../auth");

// Checkout
router.post("/checkout", auth.verify, (req, res) => {
	let data ={
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity
	};
	orderController.checkout(data).then(resultFromController => res.send(resultFromController))
});

module.exports = router;