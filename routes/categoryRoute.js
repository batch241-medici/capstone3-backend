const express = require("express");

const categoryController = require("../controllers/categoryController");

const router = express.Router();

// All Categories
router.get("/allCategories", (req, res) => {
	categoryController.allCategory().then(resultFromController => res.send(resultFromController));
});

module.exports = router;